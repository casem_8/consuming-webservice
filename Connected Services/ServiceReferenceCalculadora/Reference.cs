﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProbandoWebService.ServiceReferenceCalculadora {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReferenceCalculadora.WebServiceCalculadoraSoap")]
    public interface WebServiceCalculadoraSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Sumar", ReplyAction="*")]
        int Sumar(int numero1, int numero2);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Sumar", ReplyAction="*")]
        System.Threading.Tasks.Task<int> SumarAsync(int numero1, int numero2);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface WebServiceCalculadoraSoapChannel : ProbandoWebService.ServiceReferenceCalculadora.WebServiceCalculadoraSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WebServiceCalculadoraSoapClient : System.ServiceModel.ClientBase<ProbandoWebService.ServiceReferenceCalculadora.WebServiceCalculadoraSoap>, ProbandoWebService.ServiceReferenceCalculadora.WebServiceCalculadoraSoap {
        
        public WebServiceCalculadoraSoapClient() {
        }
        
        public WebServiceCalculadoraSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WebServiceCalculadoraSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WebServiceCalculadoraSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WebServiceCalculadoraSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int Sumar(int numero1, int numero2) {
            return base.Channel.Sumar(numero1, numero2);
        }
        
        public System.Threading.Tasks.Task<int> SumarAsync(int numero1, int numero2) {
            return base.Channel.SumarAsync(numero1, numero2);
        }
    }
}
