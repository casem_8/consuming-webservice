﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalculadoraForm.aspx.cs" Inherits="ProbandoWebService.CalculadoraForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <h1>Calculadora de sumas</h1>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="TxtNumero1" runat="server"></asp:TextBox><br />
            <asp:TextBox ID="TxtNumero2" runat="server"></asp:TextBox><br />
            <asp:Button ID="btnSumar" runat="server" Text="Sumar" OnClick="btnSumar_Click" /><br />
            <asp:TextBox ID="TxtResultado" runat="server"></asp:TextBox>

        </div>
    </form>
</body>
</html>
