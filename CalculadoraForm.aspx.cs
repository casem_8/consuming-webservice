﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProbandoWebService;

namespace ProbandoWebService
{
    public partial class CalculadoraForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSumar_Click(object sender, EventArgs e)
        {
            ServiceReferenceCalculadora.WebServiceCalculadoraSoapClient cliente = new ServiceReferenceCalculadora.WebServiceCalculadoraSoapClient();
            int resultado = cliente.Sumar(int.Parse(TxtNumero1.Text), int.Parse(TxtNumero2.Text));
            TxtResultado.Text = resultado.ToString();
        }
    }
}